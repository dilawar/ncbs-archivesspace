VERSION := 2.7.1
UPSTREAM_CODE := upstream_as
RUN_DIR := archivesspace
ZIP_FILE := archivesspace-$(VERSION).zip

all : deploy 

deploy: extract_patch build
	cd $(RUN_DIR) && ./archivesspace.sh restart && tail -f ./logs/archivesspace.out

copy_plugin : 
	# copy config.rb file to right place.
	cp -f config/config.rb $(RUN_DIR)/config/config.rb
	cp lib/mysql-connector-java-8.0.20.jar $(RUN_DIR)/lib
	cp --parents stylesheets/archivesspace.small.png $(RUN_DIR)
	cp --parents -r plugins/local/ $(RUN_DIR)/

extract_patch : build
	$(MAKE) copy_plugin

build : $(ZIP_FILE)
	unzip $(ZIP_FILE)

$(ZIP_FILE) :
	cd $(UPSTREAM_CODE) && ./scripts/build_release $(VERSION) && mv $(ZIP_FILE) ..

init_subtree :
	git subtree add --prefix $(UPSTREAM_CODE) \
		https://github.com/dilawar/archivesspace ncbs --squash

update_subtree:
	git subtree pull --prefix $(UPSTREAM_CODE) \
		https://github.com/archivesspace/archivesspace master --squash


run reload : copy_plugin
	cd $(RUN_DIR) && ./archivesspace.sh restart && tail -f ./logs/archivesspace.out
	

clean :
	rm -rf $(ZIP_FILE) $(RUN_DIR)


.PHONY : clean
