Whole process is automated using makefile. In most cases, runnning `make`
should just work fine. Make assumes that you already have mysql database setup
and user `as` has password `as123` and have access to database `archivesspace`.
See `./config/config.rb` file for more details.

The upstream source code is in folder `upstream_as`. it is added as subtree. To
update this subtree, run `make update_subtree`.

When you run `make`, it builds the APP and copy NCBS specific branding to
appropriate foldes (plugin `local`).

# Recreating seach indices

First lets check the index.

$ ./scripts/checkindex.sh data/solr_index/index

In any case, we have to delete Solr indices. Following files must be deleted after stopping archivespace processes.

- /path/to/archivespace/data/solr_index/index/
- /path/to/archivesspace/data/indexer_state
- /path/to/archivesspace/data/indexer_pui_state

http://lyralists.lyrasis.org/pipermail/archivesspace_users_group/2017-June/004864.html

Restart `archivesspace.sh start` and wait for some time for indices to get repopulated.



